package StringTask;

public class StringUtils {

    public StringUtils() {
    }

    public String getStringObject(Object... params) {
        StringBuilder str = new StringBuilder();
        for (Object p : params) {
            str.append(p).append("\n");
        }
        return str.toString();
    }

}

package StringTask;

@FunctionalInterface
public interface Printable {
    void print();
}

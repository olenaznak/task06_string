package StringTask;

import java.util.*;

public class Menu {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    Locale locale;
    ResourceBundle bundle;

    public Menu() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        methodsMenu = new LinkedHashMap<>();

        methodsMenu.put("1", this::testStringUtils);
        methodsMenu.put("2", this::testRegEx);
        methodsMenu.put("3", this::internationalizeMenuEnglish);
        methodsMenu.put("4", this::internationalizeMenuUkrainian);
        methodsMenu.put("5", this::internationalizeMenuFrench);
        methodsMenu.put("Q", this::exit);
    }

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("Q", bundle.getString("Q"));
    }

    private void testStringUtils() {
        StringUtils utils = new StringUtils();
        String name = "Helen Znak";
        int age = 19;
        String city = "Lviv";
        StringUtils su = new StringUtils();
        System.out.println(su.getStringObject(name,age,city));
    }

    private void internationalizeMenuUkrainian() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuFrench() {
        locale = new Locale("fr");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }

    private void testRegEx() {
        String str1 = "And for a while, I did.";
        String str2 = "Think I'll miss you forever like the stars miss the sun";
        String str3 = "hello, it`s me";
        System.out.println(str1);
        System.out.println(str2);
        System.out.println(str3);

        System.out.println("\nIs first letter capital and period at the end?");
        System.out.println(StringFormatter.checkFirstCapitalAndEndPeriod(str1));
        System.out.println(StringFormatter.checkFirstCapitalAndEndPeriod(str2));
        System.out.println(StringFormatter.checkFirstCapitalAndEndPeriod(str3));

        System.out.println("\nSplit on \"you\" or \"the\"");
        System.out.println(StringFormatter.splitOnTheOrYou(str1));
        System.out.println(StringFormatter.splitOnTheOrYou(str2));
        System.out.println(StringFormatter.splitOnTheOrYou(str3));

        System.out.println("\nReplace all vowels with underscores");
        System.out.println(StringFormatter.replaceAllTheVowels(str1));
        System.out.println(StringFormatter.replaceAllTheVowels(str2));
        System.out.println(StringFormatter.replaceAllTheVowels(str3));
    }

    private void exit(){
        System.out.println("Exit");
        System.exit(0);
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet()) {
            if (key.length() == 1) {
                System.out.println(menu.get(key));
            }
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}

package StringTask;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringFormatter {

    public static boolean checkFirstCapitalAndEndPeriod(String str) {
        Pattern pattern = Pattern.compile("[A-Z].*\\.");
        Matcher matcher = pattern.matcher(str);
        return matcher.matches();
    }

    public static List<String> splitOnTheOrYou(String str) {
        return Arrays.asList(str.split("(you | the)+")); //not always
    }

    public static String replaceAllTheVowels(String str) {
        return str.replaceAll("[AEOIUaeoiu]" , "_");
    }
}

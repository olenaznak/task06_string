package RegExTask;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegEx {
    public long counDuplicates() {
        String text;
        Scanner in = new Scanner(System.in);
        text = in.nextLine();

        String pattern = "\\b([A-Z]+)\\s+\\1\\b";

        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(text);

        while (m.find()){
            System.out.println(m.start() + m.end());
        }

        return 0;
    }

}
